# Server Side Request Forgery (SSRF)

## Description

A [Server-side Request Forgery (SSRF)][1] is an attack in which an attacker
is able coerce a application into making an outbound request to an unintended
resource. This resource is usually internal. In GitLab, the connection most
commonly uses HTTP, but an SSRF can be performed with any protocol, such as
Redis or SSH.

With an SSRF attack, the UI may or may not show the response. The latter is
called a Blind SSRF. While the impact is reduced, it can still be useful for
attackers, especially for mapping internal network services as part of recon.

[1]: https://www.hackerone.com/blog-How-To-Server-Side-Request-Forgery-SSRF

## Impact

The impact of an SSRF can vary, depending on what the application server
can communicate with, how much the attacker can control of the payload, and
if the response is returned back to the attacker. Examples of impact that
have been reported to GitLab include:

* Network mapping of internal services
  * This can help an attacker gather information about internal services
  that could be used in further attacks.
  * https://gitlab.com/gitlab-org/gitlab-ce/issues/51327
* Reading internal services, including cloud service metadata.
  * The latter can be a serious problem, because an attacker can obtain keys that allow control of the victim's cloud infrastructure. (This is also a good reason
  to give only necessary privileges to the token.)
  * https://gitlab.com/gitlab-org/gitlab-ce/issues/51490
* When combined with CRLF vulnerability, remote code execution.
  * https://gitlab.com/gitlab-org/gitlab-ce/issues/41293

## When to Consider
* When the application makes any outbound connection

## Mitigations

In order to mitigate SSRF vulnerabilities, it is necessary to validate the destination of the outgoing request, especially if it includes user-supplied information.

The preferred SSRF mitigations within GitLab are:
1. Only connect to known, trusted domains/IP addresses.
2. Use the [GitLab::HTTP](#gitlab-http-library) library
3. Implement [feature-specific mitigations](#feature-specific-mitigations)

### Gitlab HTTP Library
The [Gitlab::HTTP][2] wrapper library has grown to include mitigations for all of the GitLab-known SSRF vectors. It is also configured to respect the
`Outbound requests` options that allow instance administrators to block all internal connections, or limit the networks to which connections can be made.

In some cases, it has been possible to configure GitLab::HTTP as the HTTP
connection library for 3rd-party gems. This is preferrable over re-implementing
the mitigations for a new feature.
  * https://dev.gitlab.org/gitlab/gitlabhq/merge_requests/2530/diffs

[2]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/http.rb

### Feature-specific Mitigations
For situtions in which a whitelist or Gitlab:HTTP cannot be used, it will be necessary to implement mitigations directly in the feature. It is best to validate the destination IP addresses themselves, not just domain names, as DNS can be controlled by the attacker. Below are a list of mitigations that should be implemented.

**Important Note:** There are many tricks to bypass common SSRF validations. If  feature-specific mitigations are necessary, they should be reviewed by the AppSec team, or a developer who has worked on SSRF mitigations previously.

* Block connections to all localhost addresses
  * `127.0.0.1/8` (IPv4 - note the subnet mask)
  * `::1` (IPv6)
* Block connections to networks with private addressing (RFC 1918)
  * `10.0.0.0/8`
  * `172.16.0.0/12`
  * `192.168.0.0/24`
* Block connections to link-local addresses (RFC 3927)
  * `169.254.0.0/16`
  * In particular, for GCP: `metadata.google.internal` -> `169.254.169.254`
* For HTTP connections: Disable redirects or validate the redirect destination
* To mitigate DNS rebinding attacks, validate and use the first IP address received

See [url_blocker_spec.rb][3] for examples of SSRF payloads

[3]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/spec/lib/gitlab/url_blocker_spec.rb
