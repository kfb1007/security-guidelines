# XSS guidelines

## Description
Cross site scripting (XSS) is an issue where malicious javascript code gets injected into a trusted web application and executed in a client's browser. The input is intended to be data, but instead gets treated as code by the browser. 

XSS issues are commonly classified in three categories, by their delivery method: 
- [Reflected XSS](https://www.owasp.org/index.php/Types_of_Cross-Site_Scripting#Reflected_XSS_.28AKA_Non-Persistent_or_Type_II.29)
- [Persistent XSS](https://www.owasp.org/index.php/Types_of_Cross-Site_Scripting#Stored_XSS_.28AKA_Persistent_or_Type_I.29)
- [DOM XSS](https://www.owasp.org/index.php/Types_of_Cross-Site_Scripting#DOM_Based_XSS_.28AKA_Type-0.29)

## Impact

The injected client-side code is executed on the victim's browser in the context of their current session. This means the attacker could perform any same action the victim would normally be able to do through a browser. The attacker would also have the ability to:
- [log victim keystrokes](https://youtu.be/2VFavqfDS6w?t=1367)
- launch a network scan from the victim's browser
- potentially [obtain the victim's session tokens](https://youtu.be/2VFavqfDS6w?t=739)
- perform actions that lead to data loss/theft or account takeover

Much of the impact is contingent upon the function of the application and the capabilities of the victim's session. For further impact possibilities, please check out [the beef project](https://beefproject.com/).

## When to consider?

When user submitted data is included in responses to end users, which is just about anywhere.

## Mitigation

In most situations, a two-step solution can be utilized: input validation and output encoding in the appropriate context.


### Input validation

- [Input Validation](https://youtu.be/2VFavqfDS6w?t=7489)

#### Setting expectations

For any and all input fields, ensure to define expectations on the type/format of input, the contents, [size limits](https://youtu.be/2VFavqfDS6w?t=7582), the context in which it will be output. It's important to work with both security and product teams to determine what is considered acceptable input.

#### Validate input

- Treat all user input as untrusted.
- Based on the expectations you [defined above](#setting-expectations):
  - Validate the [input size limits](https://youtu.be/2VFavqfDS6w?t=7582).
  - Validate the input using a [whitelist approach](https://youtu.be/2VFavqfDS6w?t=7816) to only allow characters through which you are expecting to receive for the field.
    - Input which fails validation should be **rejected**, and not sanitized.

Note that blacklists should be avoided, as it is near impossible to block all [variations of XSS](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet).

### Output encoding

Once you've [determined when and where](#setting-expectations) the user submitted data will be output, it's important to encode it based on the appropriate context. For example:

- Content placed inside HTML elements need to be [HTML entity encoded](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#rule-1---html-escape-before-inserting-untrusted-data-into-html-element-content).
- Content placed into a JSON response needs to be [JSON encoded](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#rule-31---html-escape-json-values-in-an-html-context-and-read-the-data-with-jsonparse).
- Content placed inside [HTML URL GET parameters](https://youtu.be/2VFavqfDS6w?t=3494) need to be [URL-encoded](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#rule-5---url-escape-before-inserting-untrusted-data-into-html-url-parameter-values)
- [Additional contexts may require context-specific encoding](https://youtu.be/2VFavqfDS6w?t=2341).

## Additional info

### Mitigating XSS in Rails
- [XSS Defense in Rails](https://youtu.be/2VFavqfDS6w?t=2442)
- [XSS Defense with HAML](https://youtu.be/2VFavqfDS6w?t=2796)
- [Validating Untrusted URLs in Ruby](https://youtu.be/2VFavqfDS6w?t=3936)
- [RoR Model Validators](https://youtu.be/2VFavqfDS6w?t=7636)

### GitLab specific libraries for mitigating XSS.

#### Vue
- [isSafeURL](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/app/assets/javascripts/lib/utils/url_utility.js#L155)

### Content Security Policy
- [Content Security Policy](https://www.youtube.com/watch?v=2VFavqfDS6w&t=12991s)
- [Use nonce-based Content Security Policy for inline JavaScript](https://gitlab.com/gitlab-org/gitlab-ce/issues/65330)

### Free form input fields

#### Sanitization
- [HTML Sanitization](https://youtu.be/2VFavqfDS6w?t=5075)
- [DOMPurify](https://youtu.be/2VFavqfDS6w?t=5381)

#### `iframe` sandboxes
- [iframe sandboxing](https://youtu.be/2VFavqfDS6w?t=7043)

## Select examples of past XSS issues affecting GitLab

- [Stored XSS in user status](https://gitlab.com/gitlab-org/gitlab-ce/issues/55320)

## Developer Training
- [Introduction to XSS](https://www.youtube.com/watch?v=PXR8PTojHmc&t=7785s)
- [Reflected XSS](https://youtu.be/2VFavqfDS6w?t=603s)
- [Persistent XSS](https://youtu.be/2VFavqfDS6w?t=643)
- [DOM XSS](https://youtu.be/2VFavqfDS6w?t=5871)
- [XSS in depth](https://www.youtube.com/watch?v=2VFavqfDS6w&t=111s)
- [XSS Defense](https://youtu.be/2VFavqfDS6w?t=1685)
- [XSS Defense in Rails](https://youtu.be/2VFavqfDS6w?t=2442)
- [XSS Defense with HAML](https://youtu.be/2VFavqfDS6w?t=2796)
- [Javascript URLs](https://youtu.be/2VFavqfDS6w?t=3274)
- [URL encoding context](https://youtu.be/2VFavqfDS6w?t=3494)
- [Validating Untrusted URLs in Ruby](https://youtu.be/2VFavqfDS6w?t=3936)
- [HTML Sanitization](https://youtu.be/2VFavqfDS6w?t=5075)
- [DOMPurify](https://youtu.be/2VFavqfDS6w?t=5381)
- [Safe Client-side JSON Handling](https://youtu.be/2VFavqfDS6w?t=6334)
- [iframe sandboxing](https://youtu.be/2VFavqfDS6w?t=7043)
- [Input Validation](https://youtu.be/2VFavqfDS6w?t=7489)
- [Validate size limits](https://youtu.be/2VFavqfDS6w?t=7582)
- [RoR model validators](https://youtu.be/2VFavqfDS6w?t=7636)
- [Whitelist input validation](https://youtu.be/2VFavqfDS6w?t=7816)
- [Content Security Policy](https://www.youtube.com/watch?v=2VFavqfDS6w&t=12991s)
