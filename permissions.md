# Permissions

## Description

Application permissions are used to determine who can access what and what actions they can perform. 
For more information about the permission model at GitLab, please see [the GitLab permissions guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/permissions.md) or the [EE docs on permissions](https://docs.gitlab.com/ee/user/permissions.html). 

## Impact

Improper permission handling can have significant impacts on the security of an application. 
Some situations may reveal [sensitive data](https://gitlab.com/gitlab-com/gl-infra/production/issues/477) or allow a malicious actor to perform [harmful actions](https://gitlab.com/gitlab-org/gitlab-ee/issues/8180). 
The overall impact depends heavily on what resources can be accessed or modified improperly.

A common vulnerability when permission checks are missing is called [IDOR](https://www.owasp.org/index.php/Testing_for_Insecure_Direct_Object_References_(OTG-AUTHZ-004)) for Insecure Direct Object References.

## When to Consider

Each time you implement a new feature/endpoint, whether it is at UI, API or GraphQL level.

## Mitigations

**Start by writing tests** around permissions: unit and feature specs should both include tests based around permissions
* Fine-grained, nitty-gritty specs for permissions are good: it is ok to be verbose here
    * Make assertions based on the actors and objects involved: can a user or group or XYZ perform this action on this object?
    * Consider defining them upfront with stakeholders, particularly for the edge cases
* Do not forget **abuse cases**: write specs that **make sure certain things can't happen**
    * A lot of specs are making sure things do happen and coverage percentage doesn't take into account permissions as same piece of code is used.
    * Make assertions that certain actors cannot perform actions
* Naming convention to ease auditability: to be defined, e.g. a subfolder containing those specific permission tests or a `#permissions` block

Be careful to **also test [visibility levels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/permissions.md#feature-specific-permissions)** and not only project access rights.

Some example of well implemented access controls and tests: 
1.  [example1](https://dev.gitlab.org/gitlab/gitlab-ee/merge_requests/710/diffs?diff_id=13750#af40ef0eaae3c1e018809e1d88086e32bccaca40_43_43)
2.  [example2](https://dev.gitlab.org/gitlab/gitlabhq/merge_requests/2511/diffs#ed3aaab1510f43b032ce345909a887e5b167e196_142_155)
3.  [example3](https://dev.gitlab.org/gitlab/gitlabhq/merge_requests/3170/diffs?diff_id=17494)

**NB:** any input from development team is welcome, e.g. about rubocop rules